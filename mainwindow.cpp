#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    browser = this->centralWidget()->findChild<QPlainTextEdit *>("Browser");
    internet = this->centralWidget()->findChild<QPlainTextEdit *>("Internet");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::set_browser_box_text(QByteArray request)
{
    if (!browser->isEnabled())
    {
        browser->clear();
        browser->setEnabled(true);
    }

    browser->setPlainText(request);
}

void MainWindow::set_internet_box_text(QByteArray request)
{
    if (!internet->isEnabled())
    {
        internet->clear();
        internet->setEnabled(true);
    }

    qDebug() << "set_internet_box: " << request.size();
    internet->setPlainText(request);
}

void MainWindow::on_send_to_internet_released()
{
    browser->setEnabled(false);

    QByteArray request = browser->toPlainText().toUtf8();

    int pos = 0;
    while ((pos = request.indexOf('\n', pos)) != -1)
    {
        request.insert(pos, '\r');
        pos = pos + 2;
    }

    emit SendToInternet(request);
}

void MainWindow::on_send_to_browser_released()
{
    internet->setEnabled(false);

    QByteArray request = internet->toPlainText().toUtf8();

    int pos = 0;
    while ((pos = request.indexOf('\n', pos)) != -1)
    {
        request.insert(pos, '\r');
        pos = pos + 2;
    }

    qDebug() << "send_to_browser: " << request.size();
    emit SendToBrowser(request);
}
