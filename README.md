# Trabalho Final de Teleinformática e Redes 2

## Docente
João José Costa Gondim

## Discentes
Tiago Cruz Valadares - 14/0164120  
Vinícius Gomes de Souza - 15/0047941

## Como Rodar
Para testar o trabalho bastar executar o arquivo _Aracne_ em um terminal:

> $ ./Aracne -p port

Onde _port_ é o número da porta do servidor proxy.  
O valor padrão para a porta é 8228. Portanto, as seguintes linhas são equivalentes:

> $ ./Aracne -p 8228  
> $ ./Aracne
