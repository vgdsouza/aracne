#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "proxyserver.h"

#include <QMainWindow>
#include <QPlainTextEdit>
#include <QObject>
#include <QDebug>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void set_browser_box_text(QByteArray request);
    void set_internet_box_text(QByteArray request);

private slots:
    void on_send_to_internet_released();
    void on_send_to_browser_released();

signals:
    void SendToInternet(QByteArray request);
    void SendToBrowser(QByteArray request);

private:
    Ui::MainWindow *ui;
    QPlainTextEdit *browser;
    QPlainTextEdit *internet;
};
#endif // MAINWINDOW_H
