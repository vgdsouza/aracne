#ifndef PROXYSERVER_H
#define PROXYSERVER_H

#include "SocketConnect.h"

#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <cstring>

#include <QByteArray>
#include <QString>
#include <QDebug>
#include <QRegularExpression>
#include <QObject>

class ProxyServer : public QObject
{
    Q_OBJECT

public:
    ProxyServer(unsigned short port = 8228);

public slots:
    void start_server();
    void receive_from_browser();
    void send_to_browser(QByteArray request);
    void handle_internet(QByteArray request);

signals:
    void StartingServer();
    void NextRequest();
    void ReceivedFromBrowser(QByteArray request);
    void ReceivedFromInternet(QByteArray request);

private:
    struct browser {
        int socket;
        int new_socket;
        unsigned short port;
        unsigned int addrlen;
        struct sockaddr_in addr;
    } browser;

};

#endif // PROXYSERVER_H
