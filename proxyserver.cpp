#include "proxyserver.h"

ProxyServer::ProxyServer(unsigned short port)
{
    browser.port = port;

    if ((browser.socket = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        qDebug("falha no socket do browser");

    int OPT = 1;

    if (setsockopt(browser.socket, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &OPT, sizeof (OPT)))
        qDebug("falha no setsockopt do browser");

    browser.addr.sin_family = AF_INET;
    browser.addr.sin_addr.s_addr = INADDR_ANY;
    browser.addr.sin_port = htons(browser.port);
    browser.addrlen = sizeof (browser.addr);

    if (bind(browser.socket, (struct sockaddr*) &browser.addr, browser.addrlen) < 0)
        qDebug("falha no bind do browser");

    if (listen(browser.socket, 3) < 0)
        qDebug("falha no listen do browser");
}

void ProxyServer::start_server()
{
    if ((browser.new_socket = accept(browser.socket, (struct sockaddr*) &browser.addr, (socklen_t*) &browser.addrlen)) < 0)
        qDebug("falha no accept do browser");

    emit NextRequest();
}

void ProxyServer::receive_from_browser()
{
    QByteArray request;
    request.clear();

    char buffer[1024] = { 0 };
    long valread = 1024;


    while (valread == 1024)
    {
        qDebug() << "aguardando o browser";
        valread = read(browser.new_socket, buffer, 1024);
        if (valread < 1024)
        {
            buffer[valread] = '\0';
        }
        request.append(QByteArray::fromRawData(buffer, (int) valread));
    }

    emit ReceivedFromBrowser(request);
    qDebug() << "request recebida do browser";
}

void ProxyServer::send_to_browser(QByteArray request)
{
    qDebug() << request.size();

    send(browser.new_socket, request.data(), strlen(request.data()), 0);

    emit NextRequest();
    qDebug() << "pedindo nova request ao browser";
}


void ProxyServer::handle_internet(QByteArray request)
{
    char buffer[1024];
    long valread = 1024;

    QRegularExpression regex("Host: ([\\S\\s]*?)[\\r\\n]");
    struct addrinfo *result;
    struct addrinfo hints;

    hints.ai_family = AF_INET;
    hints.ai_socktype = 0;
    hints.ai_flags = (AI_V4MAPPED | AI_ADDRCONFIG);
    hints.ai_protocol = 0;
    hints.ai_canonname = nullptr;
    hints.ai_addr = nullptr;
    hints.ai_next = nullptr;

    if (getaddrinfo(regex.match(QString::fromUtf8(request)).captured(1).toLocal8Bit().constData(), nullptr, &hints, &result) != 0)
        qDebug("falha ao resolver (getaddrinfo) o hostname da internet");

    char hostname[1024];

    if (getnameinfo(result->ai_addr, result->ai_addrlen, hostname, 1024, nullptr, 0, NI_NUMERICHOST) != 0)
        qDebug("falha ao resolver (getnameinfo) o hostname da internet");

    freeaddrinfo(result);

    int internet_fd;
    if ((internet_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        qDebug("falha no socket da internet");

    struct sockaddr_in internet_addr;
    internet_addr.sin_family = AF_INET;
    internet_addr.sin_port = htons(80);

    if (inet_pton(AF_INET, hostname, &internet_addr.sin_addr) <= 0)
        qDebug("falha no inet_pton da internet");

    unsigned int internet_addrlen = sizeof (internet_addr);

    if (SocketConnect::socket_connect(internet_fd, (struct sockaddr*) &internet_addr, internet_addrlen) < 0)
        qDebug("falha no connect da internet");

    send(internet_fd, request.data(), strlen(request.data()), 0);

    request.clear();

    while (valread == 1024)
    {
        valread = read(internet_fd, buffer, 1024);
        if (valread < 1024)
        {
            buffer[valread] = '\0';
        }
        request.append(QByteArray::fromRawData(buffer, (int) valread));
    }

    qDebug() << request.size();

    emit ReceivedFromInternet(request);
}
