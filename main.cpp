#include "mainwindow.h"
#include "proxyserver.h"

#include <QApplication>
#include <QThread>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    unsigned short port = 8228;

    if (argc == 3 && argv[1][0] == '-' && argv[1][1] == 'p') {
            port = (unsigned short) atoi(argv[2]);
    }

    ProxyServer proxy(port);

    QThread proxyThread;

    proxy.moveToThread(&proxyThread);
    proxyThread.connect(&proxy, SIGNAL(StartingServer()), &proxy, SLOT(start_server()), Qt::QueuedConnection);
    proxyThread.connect(&proxy, SIGNAL(NextRequest()), &proxy, SLOT(receive_from_browser()), Qt::QueuedConnection);
    proxyThread.connect(&proxy, SIGNAL(ReceivedFromBrowser(QByteArray)), &w, SLOT(set_browser_box_text(QByteArray)), Qt::QueuedConnection);
    proxyThread.connect(&w, SIGNAL(SendToInternet(QByteArray)), &proxy, SLOT(handle_internet(QByteArray)), Qt::QueuedConnection);
//    proxyThread.connect(&proxy, SIGNAL(ReceivedFromInternet(QByteArray)), &w, SLOT(set_internet_box_text(QByteArray)), Qt::QueuedConnection);
//    proxyThread.connect(&w, SIGNAL(SendToBrowser(QByteArray)), &proxy, SLOT(send_to_browser(QByteArray)), Qt::QueuedConnection);
    proxyThread.connect(&proxy, SIGNAL(ReceivedFromInternet(QByteArray)), &proxy, SLOT(send_to_browser(QByteArray)), Qt::QueuedConnection);
    proxyThread.start();

    emit proxy.StartingServer();

    return a.exec();
}
