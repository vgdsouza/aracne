#ifndef SOCKETCONNECT_H
#define SOCKETCONNECT_H

#include <sys/socket.h>

class SocketConnect
{
public:
    static int socket_connect(int sock, struct sockaddr *addr, unsigned int addrlen)
    {
        return connect(sock, addr, addrlen);
    }
};

#endif // SOCKETCONNECT_H
